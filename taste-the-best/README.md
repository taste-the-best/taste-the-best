# Taste the Best
Cooking Forum
# Logo
![home view](./src/images/logo-big.png "Text to show on mouseover")


# Mockups

## Home view

![home view](./src/images/readme-images/public-view.jpg "home view")
![home view](./src/images/readme-images/private-view.jpg "home view")

## Private view
![recipes view](./src/images/readme-images/recipes.jpg "recipes view")

### Single view
![single view](./src/images/readme-images/single-view.jpg "single view")

### Upload view
![upload view](./src/images/readme-images/upload-view.jpg "upload view")

### About view
![about view](./src/images/readme-images/about-view.jpg "about view")


# Final Project

## Unregistered user
![Unregistered user](./src/images/readme-images/Screenshot_1.jpg "Unregistered user")

## Register
![Register](./src/images/readme-images/Screenshot_2.jpg "Register")

## Log in
![Log in](./src/images/readme-images/Screenshot_3.jpg "Log in")

## Recipes
![Recipes](./src/images/readme-images/Screenshot_4.jpg "Recipes")

## All recipes, Favorite, Most Popular, New Recipes, Search by Name, Filter by Category
![All recipes](./src/images/readme-images/Screenshot_4.jpg "All recipes")


## My Uploaded Recipes
![My Uploaded Recipes](./src/images/readme-images/Screenshot_5.jpg "My Uploaded Recipes")

## Upload Recipe
![Upload](./src/images/readme-images/Screenshot_6.jpg "Upload")

## Edit Recipe
![Edit Recipe](./src/images/readme-images/Screenshot_14.jpg "Edit Recipe")

## View Details
![View Details](./src/images/readme-images/Screenshot_12.jpg "View Details")

## Comments
![Comments](./src/images/readme-images/Screenshot_13.jpg "Comments")

## Profile
![Profile](./src/images/readme-images/Screenshot_7.jpg "Profile")

## Edit Profile
![Edit Profile](./src/images/readme-images/Screenshot_8.jpg "Edit Profile")

## Search Users
![Search Users](./src/images/readme-images/Screenshot_9.jpg "Search Users")

## User Info
![User Info](./src/images/readme-images/Screenshot_10.jpg "User Info")

## About
![About](./src/images/readme-images/Screenshot_11.jpg "About")



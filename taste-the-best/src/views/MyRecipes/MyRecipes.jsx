import React, { useContext, useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom'
import AppContext from '../../providers/AppContext'
import { getAllRecipes } from '../../services/recipes.service'

const MyRecipes = () => {
  const { userData }= useContext(AppContext)
  const [recipes, setRecipes] = useState([])

  useEffect (()=> {
    getAllRecipes()
    .then((e)=> e.filter(el=> el.author === userData.handle))
    .then(setRecipes)
    .catch(console.error)
  }, [])
  
  return (
    <div>
      <div className='Recipes'>
         
         {recipes.length === 0
         ? <p>No recipes to show.</p>
          : recipes.map(recipe =>  {
          return <div key={recipe?.content?.imageURL} className='SingleView'>
            <div className='SingleView'>
            <h3>{recipe?.content?.name}</h3>
            <img src={recipe?.content?.imageURL} alt='recipe' className='SingleImage'></img>
            <NavLink to={`../recipes/all-recipes/${recipe.id}` } className='ViewDetailsLink' >View Details</NavLink>
            </div>
          </div> })
         }
      </div>
    </div>
  )
}

export default MyRecipes
import React, { useEffect, useState } from 'react';
import './Recipes.css'
import image from './../../images/search-icon.png'
import { BrowserRouter, Link, Navigate, NavLink, Route, Routes, useNavigate } from 'react-router-dom';
import AllRecipes from '../AllRecipes/AllRecipes';
import Upload from '../Upload/Upload';
import Favorites from '../Favorites/Favorites';
import SearchBar from '../../components/SearchBar/SearchBar';
import Users from '../../components/Users/Users';

const Recipes = () => {


  
  return (
    <div className = "Recipes">
      <div className="RecipesSubHeader">
        <SearchBar/>
        <div className="RecipesLinks">
          <NavLink to={'all-recipes'} className="navLink">All recipes</NavLink>
          <NavLink to={'favorites'} className="navLink">Favorites</NavLink>
          <NavLink to={'most-popular'} className="navLink">Most Popular</NavLink>
          <NavLink to={'new-recipes'} className="navLink">New Recipes</NavLink>
        </div>
      </div>

      <div className="RecipesContainer">

      </div>
 

  </div>
 
  )
}

export default Recipes
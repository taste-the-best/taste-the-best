import { useState } from 'react';
import './Register.css';
import { registerUser } from '../../services/auth.service';
import { getUserByHandle, createUserHandle } from '../../services/users.service';
import { useNavigate } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import validator from 'validator';
import image from './../../../src/images/logo.png';



const Register = () => {
  const [form, setForm] = useState({
    email: '',
    password: '',
    handle: '',
    firstName: '',
    lastName:'',
    profilePicture: image,
  });

  const [wrongPassword, setWrongPassword] = useState('');
  const [wrongHandle, setWrongHandle] = useState('');
  const [wrongEmail, setWrongEmail] = useState('');
  const [wrongFirstName, setWrongFirstName] = useState('');
  const [wrongLastName, setWrongLastName] = useState('');
  const [wrongImageURL, setWrongImageURL] = useState('');

  const navigate = useNavigate();

  const updateForm = prop => e => {

    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };


  const register = (e) => {
    e.preventDefault();

    
    const mailFormat = /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/;

    if (!form.email.match(mailFormat)) {
      setWrongEmail(` * Email ${form.email} is not valid!`);
    } 


    if (form.password.length < 6) {
      setWrongPassword('* Please use a strong password with minimum 6 symbols!');
      return;
    }

    if (form.firstName.length < 4 || form.firstName.length > 32) {
      setWrongFirstName('* Please add a first name between 4 and 32 symbols!');
      return;
    }

    if (form.lastName.length < 4 || form.lastName.length > 32) {
      setWrongLastName('* Please add a last name between 4 and 32 symbols!');
      return;
    }


    getUserByHandle(form.handle)
      .then(snapshot => {

        if (snapshot.exists()) {
          setWrongHandle(` * Already exists username @${form.handle}, please try another one!`);
          return;
        }

        return registerUser(form.email, form.password)
          .then(u => {
            createUserHandle(form.handle, u.user.uid, u.user.email, form.firstName, form.lastName, form.profilePicture)
            .then(() => {
              navigate('/home');
            })
            .catch(console.error);
          })
          .catch(e => {
            if (e.message.includes(`email-already-in-use`)) {
              setWrongEmail(` * Email ${form.email} has already been registered!`);
              return;
            }

          });
      })
      .catch(console.error);
  };

  return (
    <div className='Register'>
      <div className='Form'>

        
        <label htmlFor='firstName'>First Name: </label>
        <input type="text" id="firstName" value={form.firstName} onChange={updateForm('firstName')}></input><br />
        <p className = 'wrongInput'>{wrongFirstName}</p>

        <label htmlFor='lastName'>Last Name: </label>
        <input type="text" id="lastName" value={form.lastName} onChange={updateForm('lastName')}></input><br />
        <p className = 'wrongInput'>{wrongLastName}</p>

        <label htmlFor='email'>Email: </label>
        <input type="email" id="email" value={form.email} onChange={updateForm('email')}></input><br />
        <p className = 'wrongInput'>{wrongEmail}</p>

        <label htmlFor='handle'>Username: </label>
        <input type="text" id="handle" value={form.handle} onChange={updateForm('handle')}></input><br />
        <p className = 'wrongInput'>{wrongHandle}</p>

        <label htmlFor='password'>Password: </label>
        <input type="password" id="password" value={form.password} onChange={updateForm('password')}></input><br /><br />
        <p className = 'wrongInput'>{wrongPassword}</p>
        

        <Button variant="success" className='ButtonWithBottomMargin' onClick={register}>Register</Button>
      </div>
    </div>
  );
};

export default Register;

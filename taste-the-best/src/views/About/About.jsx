import React from 'react'
import './About.css';
import Toni from './../../images/Toni.jpg';
import Zhorzh from './../../images/Zhorzh.png';

const About = () => {
  return (
    <div className='About'>
      <h1 id='AboutH1'>Telerik Academy Project 2</h1>
      <h2 id='ProjectName'>&#9733; &#9733; &#9733;   Taste the Best   &#9733; &#9733; &#9733;</h2>


      <div class="authors">
        <div class = "singleAuthor">
          <img class="authorImg" src={Toni} alt='Toni'></img>
          <p class="authorName"> Antoniya Lambova </p>
          <a href="mailto:adelfia@gmail.com" class="email">E-mail: adelfia@gmail.com</a>
          <p><a href="tel:+359887985456" class="phone">Phone number: +359 887 985 456</a></p>
        </div>

        <div class = "singleAuthor">
          <img class="authorImg" src={Zhorzh} alt='Toni'></img>
          <p class="authorName"> Zhorzh Tomov </p>
          <a href="mailto:zh_tomov@hotmail.com" class="email">E-mail: zh_tomov@hotmail.com</a>
          <p><a href="tel:+359886854333" class="phone">Phone number: +359 886 854 333</a></p>
        </div>

      </div>


    </div>
  )
}

export default About
import { getAuth, updateProfile , updateEmail , reauthenticateWithCredential} from 'firebase/auth';
import { getDownloadURL, ref, uploadBytes } from 'firebase/storage';
import React, { useContext,  useState , useEffect} from 'react'
import { Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { storage } from '../../config/firebase-config';
import AppContext from '../../providers/AppContext';
import { registerUser } from '../../services/auth.service';
import { createUserHandle, updateUser } from '../../services/users.service';
import image from './../../../src/images/logo.png';
import './EditProfile.css';

const EditProfile = () => {
    const { userData } = useContext(AppContext);
    const navigate = useNavigate();
    const [form, setForm] = useState({
        email: '',
        handle: '',
        firstName: '',
        lastName:'',
        profilePicture: image,
        uid: '',
      });
    

    const [wrongFirstName, setWrongFirstName] = useState('');
    const [wrongLastName, setWrongLastName] = useState('');
    const [wrongProfilePicture, setWrongProfilePicture] = useState('');
     
    const updateForm = prop => e => {

        setForm({
          ...form,
          [prop]: e.target.value,
        });
      };

      const register = (e) => {
        e.preventDefault();
    

    
        if (form.firstName.length < 4 || form.firstName.length > 32) {
          setWrongFirstName('* Please add a first name between 4 and 32 symbols!');
          return;
        }
    
        if (form.lastName.length < 4 || form.lastName.length > 32) {
          setWrongLastName('* Please add a last name between 4 and 32 symbols!');
          return;
        }
        if (form.profilePicture === image) {
          setWrongProfilePicture('* Please insert picture')
          return;
        }
       
        
      
          updateUser(form.handle, form.firstName, form.lastName, form.email, form.profilePicture)
          navigate("/")
         
          
        }


    const uploadPicture = (e) => {
      e.preventDefault()

      const file= e.target[0]?.files?.[0];
      if(!file) return alert(`Please insert picture`)
      const picture = ref(storage, `profilePictures/${form.handle}`)

      uploadBytes(picture, file)
      .then(snapshot=>{
        return getDownloadURL(snapshot.ref)
        .then(url => {
  
          setForm({...form,
            profilePicture: url})
        })
      })
      .catch(console.error)
    }

    useEffect(()=>{
        setForm({
            email: userData?.email,
            handle: userData?.handle,
            firstName: userData?.firstName,
            lastName: userData?.lastName,
            profilePicture: userData?.profilePicture,
            uid: userData?.uid
        })
    },[userData?.email, userData?.password, userData?.handle, userData?.firstName, userData?.lastName, userData?.uid, userData?.profilePicture])
        
    
    
  return (
    <div>
       <div className='NameInput'>
          <input type="text" id="name" name="name-input" onChange={updateForm('firstName')} value={form.firstName} placeholder="Insert Name of First Name" ></input>
          <p className = 'wrongInput'>{wrongFirstName}</p>
        </div>
        <div className='NameInput'>
          <input type="text" id="name" name="name-input" onChange={updateForm('lastName')} value={form.lastName} placeholder="Insert Name of Last Name" ></input>
          <p className = 'wrongInput'>{wrongLastName}</p>
        </div>
        <div className ='UploadProfileImage' >
          <form onSubmit={uploadPicture}>
          <img src={form?.profilePicture} alt ='profile' className='PreviewPicture'/>
          <p className = 'wrongInput'>{wrongProfilePicture}</p>
          <input type="file" name='file' />
          
          <button type='submit' className='UploadImageButton'>Upload Image</button>
          </form>
        </div>
        <Button variant="success" id='UpdateProfileButton' onClick={register}>Update</Button>
    </div>
  )
}

export default EditProfile
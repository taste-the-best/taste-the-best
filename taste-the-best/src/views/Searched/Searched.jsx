import React, { useEffect, useState } from 'react'
import './Searched.css';
import { NavLink, useLocation, useParams, useSearchParams } from 'react-router-dom';
import LinksToRecipes from '../../components/LinksToRecipes/LinksToRecipes';
import SearchBar from '../../components/SearchBar/SearchBar';
import Users from '../../components/Users/Users';
import { getAllRecipes } from '../../services/recipes.service';
import { nanoid } from 'nanoid'

const Searched = () => {
  
    const [recipes, setRecipes] = useState([])
    const location = useLocation();
    const test = new URLSearchParams(location.search).get("q")
   
    useEffect (()=> {
    
        getAllRecipes()
        .then((e)=>e.filter(el => {
          if(el.content.name === null || el.content.name === undefined) {
            return null
          }else if (el.content.name.toLowerCase().includes(test)) {
            return el
          }
        }))
        .then(setRecipes)
        .catch(console.error)
      }, [test])

  return (
    <div>
      <Users/>
      <SearchBar/>
      <LinksToRecipes/>
      
      <div className='Recipes'>
        
        {recipes.length === 0
        ? <p>No recipes to show.</p>
          : recipes.map(recipe =>  {
          return <div  className='SingleView' key={nanoid(4)}>
           
            <h3>{recipe?.content?.name}</h3>
            <img src={recipe?.content?.imageURL} alt='recipe' className='SingleImage'></img>
            <NavLink to={`../recipes/all-recipes/${recipe.id}` } className='ViewDetailsLink' >View Details</NavLink>
            
          </div> })
        }
      </div>
  </div>
  )
}

export default Searched
import './Home.css'
import React, { useContext, useEffect, useState } from 'react'
import Users from '../../components/Users/Users'
import { getAllRecipes } from '../../services/recipes.service'
import { NavLink } from 'react-router-dom'
import AppContext from '../../providers/AppContext'

const Home = () => {
  const { userData }= useContext(AppContext)
  const [recipes, setRecipes] = useState([])
  
  useEffect (()=> {
    getAllRecipes()
    .then(setRecipes)
    .catch(console.error)
  }, [])

  recipes.sort(function (a, b) {
    return   +(b.content.rating) - +(a.content.rating)
  })
 
  return (
    <div className='Home'>
      <Users />
      <div className='Recipes' id='homeRecipes'>
         <h1 className='Title'>Top Recipes</h1>
         { recipes.sort(function (a, b) {
              return   +(b.content.rating) - +(a.content.rating)
            }).slice(0,9).map(recipe =>  {
          return <>
            <div key={recipe?.content?.imageURL} className='SingleView'>
            <h3>{recipe?.content?.name}</h3>
            <img src={recipe?.content?.imageURL} alt='recipe' className='SingleImage'></img>
            {userData?.handle
            ?<NavLink to={`../recipes/all-recipes/${recipe.id}` } className='ViewDetailsLink' >View Details</NavLink>
            :<div id='parent'><p className='pleaseLogIn'>* to see details please login</p></div>}
            </div>
          </> })
         }
      </div>
      <div className='Recipes' >
        <h1 className='Title'>Last Recipes</h1>
        {  recipes.sort(function (a, b) {
              return   b.createdOn - a.createdOn
            }).slice(0,9).map(recipe =>  {
          return <>
            <div key={recipe?.content?.imageURL}className='SingleView'>
            <h3>{recipe?.content?.name}</h3>
            <img src={recipe?.content?.imageURL} alt='recipe' className='SingleImage'></img>
            {userData?.handle
            ?<NavLink to={`../recipes/all-recipes/${recipe.id}` } className='ViewDetailsLink' >View Details</NavLink>
            :<div id='parent'><p className='pleaseLogIn'>* to see details please login</p></div>}
            </div>
          </> })
         }
      </div>
    </div>
  )
}

export default Home
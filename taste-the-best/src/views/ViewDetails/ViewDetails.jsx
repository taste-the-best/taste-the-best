import './ViewDetails.css';
import React, { useContext, useEffect, useState } from 'react'
import Muffins0 from '../../components/Muffins/Muffins0';
import Muffins1 from '../../components/Muffins/Muffins1';
import Muffins2 from '../../components/Muffins/Muffins2';
import Muffins3 from '../../components/Muffins/Muffins3';
import Muffins4 from '../../components/Muffins/Muffins4';
import Muffins5 from '../../components/Muffins/Muffins5';
import like from './../../../src/images/like.png';
import likePressed from './../../../src/images/like-pressed.png';
import dislike from './../../../src/images/dislike.png';
import dislikePressed from './../../../src/images/dislike-pressed.png';
import love from './../../../src/images/love.png';
import lovePressed from './../../../src/images/love-pressed.png';
import Button from 'react-bootstrap/Button';
import { useNavigate, useParams } from 'react-router-dom';
import { getRecipeById, likeRecipes , removeLikeRecipe , dislikeRecipes , removeDislikeRecipe, getAllLiked, getAllDisliked, deleteRecipe, getFavoriteStatus, checkIsLikedByHandle, checkIsDislikedByHandle, setRatingDataFunction, getRating} from '../../services/recipes.service';
import { addUserFavorites, removeUserFavorites} from '../../services/users.service';
import AppContext from '../../providers/AppContext';
import { addComment, commentRecipes, getAllComments} from '../../services/comments.service';
import { v4 } from 'uuid';
import CreateComment from '../../components/CreateComment/CreateComment';
import AllComments from '../../components/AllComments/AllComments';


const ViewDetails = () => {
  const navigate = useNavigate();

  const { id } = useParams()
  const { userData: { handle, profilePicture} } = useContext(AppContext);

  const [likes, setLikes] = useState([0]);
  const [dislikes, setDislikes] = useState([0]);
  const [fav, setFav] = useState();
  const [recipe, setRecipe] = useState({});
  const { userData }= useContext(AppContext);
  const [liked, setLiked] = useState();
  const [disliked, setDisliked] = useState();
  const [idOfComment, setIdOfComment] = useState('');
  const [done, setDone] = useState('');
  const [emptyComment, setEmptyComment] = useState('');
  const [ratingData, setRatingData] = useState();
  const [comment, setComment] = useState([]);
  const [comments, setComments] = useState([]);

  const result = {
    textComment: comment,
    recipeId: recipe.id,
    likedBy: [],
  }

  useEffect(() => {
    if(isNaN(comments.length)) {
      setComments([])
    }

    if(comments.length === 0 ){
      setComment('');
    } 

    getAllComments(id)

        .then(setComments)
        .catch(console.error);
  }, [idOfComment, id]);


    const createComment = (content) => {
      setEmptyComment('');
      setDone('');
      
      const nameOfComment = v4();
      setIdOfComment(nameOfComment);
  
      const idOfRecipe = result.recipeId;
  
      commentRecipes(handle, nameOfComment, idOfRecipe );

      return addComment(content, handle, nameOfComment, idOfRecipe, profilePicture)
        .then(comment2 => {
          setComments([comment2, ...comments]);
        });
    }
  

  useEffect(()=> {
    checkIsLikedByHandle(id, handle)
    .then(result => {
      setLiked(result);
    });
  }, [liked, id, handle]);

  useEffect(()=> {
    checkIsDislikedByHandle(id, handle)
    .then(result => {
      setDisliked(result);
    });
  }, [disliked, id, handle]);

  useEffect(()=> {
  getAllLiked(id)
  .then(setLikes)
  .catch(console.err)
  }, [liked, id]);

  useEffect(()=> {
  getAllDisliked(id)
  .then(setDislikes)
  .catch(console.err)
  }, [disliked, id]);

  useEffect(()=> {
    if(isNaN(likes.length)) {
      setLikes([])
    }
    if(isNaN(dislikes.length)) {
      setDislikes([])
    }
    if(likes.length===0 && dislikes.length ===0 ){
      setRatingData(0);
      setRatingDataFunction(id, 0)
    } else {
      let rating = (likes.length*100)/(likes.length+dislikes.length);
      setRatingData(rating);
      setRatingDataFunction(id, rating);
    }

    }, [likes, dislikes, id]);


  const AddRemoveFavorite = () => { 
  
      if (fav === true) {
        removeUserFavorites(userData.handle , id)
        setFav(false);
      } else {
        addUserFavorites(userData.handle , id)
        setFav(true);
      }
  }

  const AddLikeRecipe = () => {
    
    if (liked === true) {
      removeLikeRecipe(handle, id); 
      setLiked(false);

      dislikeRecipes(userData.handle , id)
      setDisliked(true);

      
    } else {
      likeRecipes(handle, id)
      setLiked(true);
      removeDislikeRecipe(userData.handle , id)
      setDisliked(false);

    }
  }

  const AddDislikeRecipe = () => {


    if (disliked === true) {
      removeDislikeRecipe(userData.handle , id)
      setDisliked(false);
  
      likeRecipes(handle, id)
      setLiked(true);

    } else {
      dislikeRecipes(userData.handle , id)
      setDisliked(true);

      removeLikeRecipe(handle, id); 
      setLiked(false);


    }
  }

  const FillMuffinsBar = () => {
    
    if (ratingData === 0) {
      return (
        <Muffins0 />
      );
    } else if (ratingData < 20) {
      return (
        <Muffins1 />
      );
    } else if (ratingData < 40) {
      return (
        <Muffins2 />
      )
    } else if (ratingData < 60) {
      return (
        <Muffins3 />
      ) 
    } else if (ratingData < 80) {
      return (
        <Muffins4 />
      ) 
    } else {
      return (
        <Muffins5 />
      ) 
    }
  }

  const dateCreatedOn = () => {
    const date = recipe?.createdOn;
    if (date !== undefined) {
      return date.toString().split(' ').splice(0, 5).join(' ');
    }
  }

  const ingredientsFormat = () => {
    const rowDataIngredients = recipe?.content?.ingredients;
    if (rowDataIngredients !== undefined) {
      const separatedDataIngredients = rowDataIngredients.split('\n');
      const listIngredients = separatedDataIngredients.map((row, index) => <li key={index}>{row}</li>)
      return listIngredients;
    }
  }

  const directionsFormat = () => {
    const rowDataIngredients = recipe?.content?.directions;
    if (rowDataIngredients !== undefined) {
      const separatedDataIngredients = rowDataIngredients.split('\n');
      const listIngredients = separatedDataIngredients.map((row, index) => <p>{row}<br></br></p>)
      return listIngredients;
    }
  }
  const deleteRec = () => {
    deleteRecipe(recipe.id)
    navigate('/recipes')
  }

  const goToEdit = (id) => {
    navigate(`/edit/${id}`)
  }

  useEffect(()=> {
    getRecipeById(id)
    .then(setRecipe)
    .catch(console.error)

    getFavoriteStatus(id, handle)
    .then(setFav)
    .catch(console.error)

    getRating(id)
    .then(setRatingData)
    .catch(console.error)
    
  }, [])



  return (
    <div className='ViewDetails'>
      <div className='ViewDetailsTopContainer'>
        <div className ='ViewDetailsImage'>
            <img src={recipe?.content?.imageURL} alt='Meal' />
        </div>

        <div className='RatingComponent'>

          <div className='LikeHolder'>
              {liked
              ?  <img src={likePressed} alt='like icon' onClick={AddLikeRecipe}/>
              :  <img src={like} alt='like icon' onClick={AddLikeRecipe}/>
              }
              <p> {likes.length} likes </p>
          </div>

          <div className='LikeHolder'>
            {disliked
            ?  <img src={dislikePressed} alt='dislike icon' onClick={AddDislikeRecipe}/>
            :  <img src={dislike} alt='dislike icon' onClick={AddDislikeRecipe}/>
            }
            <p> {dislikes.length} dislikes </p>
          </div>

          
          <div className='LikeHolder'>
          
              {fav ? 
                <>
                  <img src={lovePressed} alt='love icon' onClick={AddRemoveFavorite}/>
                  <p>Remove from favorite</p>
                  
                </> :
                <>  
                  <img src={love} alt='love icon' onClick={AddRemoveFavorite}/>
                  <p>Add to favorites</p> 
                </>
              }
           
          </div>

        </div>

        <div className='DetailsInfo'>
          
            {FillMuffinsBar()}

            <p>Name:<span> {recipe?.content?.name} </span></p>
            <p>Author:<span> {recipe?.author}</span></p>
            <p>Date created: <span>{dateCreatedOn()}</span></p>
            <p>Category: <span>{recipe?.content?.category}</span></p>
            <p>Difficulty:<span> {recipe?.content?.difficulty} </span></p>
            <p>Time to cook:<span> {recipe?.content?.time}</span></p>
            {recipe.author === handle?<Button variant='outline-success' className='EditButton' onClick={()=>goToEdit(id)}>Edit your recipe</Button>:null}
            {recipe.author === handle || userData.role === 3? <Button variant='outline-danger' className='DeleteButton' onClick={deleteRec}>Delete</Button> : null }
         
        </div>
      </div>

      <div className='RecipeTextContainer'>
        <div className='Ingredients'>
          <h2> Ingredients: </h2>
          <p> {ingredientsFormat()}</p>
        </div>

        <div className='Directions'>
          <h2> Directions: </h2>
          <p>{directionsFormat()}</p>
        </div>
      </div>

      <div className='AllComments'>
        <AllComments comments={comments} setComments={setComments} />       
      </div>

      {userData.role !== 2 
      ?
        <div className='InsertComment'>
          <CreateComment onSubmit={createComment} />
          <p className = 'Done'>{done}</p>
          <p className = 'EmptyComment'>{emptyComment}</p>
  
        </div>
      :null}

    </div>

  )
}


export default ViewDetails
import React, { useContext, useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom'
import LinksToRecipes from '../../components/LinksToRecipes/LinksToRecipes'
import SearchBar from '../../components/SearchBar/SearchBar'
import Users from '../../components/Users/Users'
import AppContext from '../../providers/AppContext'
import { getAllRecipes, getRecipeById } from '../../services/recipes.service'
import { getAllFavorite } from '../../services/users.service'
import { nanoid } from 'nanoid';
import './Favorites.css';

const Favorites = () => {
  const { userData: { handle } } = useContext(AppContext);
  const [recipes, setRecipes] = useState([])
  
  useEffect (()=> {
    
    getAllRecipes()
    .then((e)=>e.filter(el => {
      if(el.content.addedToFavoritesBy === null || el.content.addedToFavoritesBy === undefined) {
        return null
      }else if (el.content.addedToFavoritesBy[handle]) {
        return el
      }
    }))
    .then(setRecipes)
    .catch(console.error)
  }, [])
  
  
  return (
    <div>
      <Users/>
       <SearchBar/>
       <LinksToRecipes/>
       <div className='Recipes'>
         
         {recipes.length === 0
         ? <p>No recipes to show.</p>
          : recipes.map(recipe =>  {
          return <div className='SingleView' key={nanoid(4)}>
            
            <h3>{recipe?.content?.name}</h3>
            <img src={recipe?.content?.imageURL} alt='recipe' className='SingleImage'></img>
            <NavLink to={`../recipes/all-recipes/${recipe.id}` } className='ViewDetailsLink' >View Details</NavLink>
            
          </div> })
         }
      </div>
    </div>
  )
}

export default Favorites
import './Upload.css'
import React, { useContext, useEffect, useState } from 'react'
import image from './../../../src/images/logo-big.png';
import { Dropdown } from 'react-bootstrap';
import DropDownTime from '../../components/DropDown/DropDownTime/DropDownTime';
import DropDownDifficulty from '../../components/DropDown/DropDownDifficulty/DropDownDifficulty';
import DropDownCategory from '../../components/DropDown/DropDownCategory/DropDownCategory';
import { storage } from '../../config/firebase-config';
import { ref , uploadBytes , list , getDownloadURL} from 'firebase/storage';
import { v4 } from 'uuid';
import { addRecipe, getAllRecipes } from '../../services/recipes.service';
import AppContext from '../../providers/AppContext';
import { useNavigate } from 'react-router-dom';


const Upload = () => {
  
  const [form, setForm] = useState({
    name: '',
    time: 'Select Time',
    category: 'Category',
    difficulty: 'Difficulty',
    ingredients:'',
    directions:'',
    rating: 0,
    likedBy: '',
    dislikedBy: '',
    imageURL: image,
    commentsIds: '',
    likedBy:'',
    dislikedBy:'',
    addedToFavoritesBy:'',
    commentedBy:'',
    
  });

  const [wrongName, setWrongName] = useState('');
  const [wrongTime, setWrongTime] = useState('');
  const [wrongDifficulty, setWrongDifficulty] = useState('');
  const [wrongCategory, setWrongCategory] = useState('');
  const [wrongIngredients, setWrongIngredients] = useState('');
  const [wrongDirections, setWrongDirections] = useState('');
  const [wrongImageURL, setWrongImageURL] = useState('');


  const updateForm = prop => e => {

    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const navigate = useNavigate()
  const register = (e) => {
    e.preventDefault();

    if (form.name.length < 5 || form.name.length > 64) {
      setWrongName('* Please add a name between 16 and 64 symbols!')
      return;
    }
    if (form.ingredients.length < 32 || form.ingredients.length > 8192) {
      setWrongIngredients('* Please add ingredients between 32 and 8192 symbols!')
      return;
    }
    if (form.directions.length < 32 || form.directions.length > 8192) {
      setWrongDirections('* Please add directions between 32 and 8192 symbols!')
      return;
    }
    if (form.category === 'Category') {
      setWrongCategory('* Please chose category')
      return;
    }
    if (form.time === 'Select Time') {
      setWrongTime('* Please chose time')
      return;
    }
    if (form.difficulty === 'Difficulty') {
      setWrongDifficulty('* Please chose difficulty')
      return;
    }
    if (form.imageURL === image) {
      setWrongImageURL('* Please insert picture')
      return;
    }

    addRecipe(form, userData.handle, idOfRecipe)
     .then(recipe => {
        setRecipes([recipe, ...recipes])
        
        navigate(`../recipes/all-recipes/${idOfRecipe}`)
       
     })
  };


  const [time, setTime] = useState('Select Time')
  const [value, setValue] = useState('Difficulty')
  const [category, setCategory] = useState('Category')
  const { userData }= useContext(AppContext)
  const [recipes , setRecipes] = useState([])
  const [idOfRecipe, setIdOfRecipe] = useState('')

  
  const uploadPicture =(e) => {
    e.preventDefault();
    
    const file= e.target[0]?.files?.[0];
    
    if(!file) return alert(`Please insert picture`)
    const nameOfRecipe = v4()
    setIdOfRecipe(nameOfRecipe)
    console.dir(file)
    const picture = ref(storage, `images/${nameOfRecipe}`)

    uploadBytes(picture, file)
    .then(snapshot=>{
      return getDownloadURL(snapshot.ref)
      .then(url => {

        setForm({...form,
        imageURL: url})
      })
    })
    .catch(console.error)

  }


  useEffect (()=> {
    getAllRecipes()
    .then(setRecipes)
    .catch(console.error)
  },[])
  

  return (
    <div className='Upload' >
      <form onSubmit={uploadPicture}>
      <div className='TopPart'>
        
        <div className ='UploadImage'>
          {form.imageURL === image ? <img src={image} alt= 'change'/> : <img src={form.imageURL} alt ='change' className='UploadImage'/>}
          <p className = 'wrongInput'>{wrongImageURL}</p>
          <input type="file" name='file' />
          <button type='submit' className='UploadFileRecipePicture'>Upload Image</button>
        </div>
        
        <div className='InsertBar'>
          <div className='NameInput'>
            <input type="text" id="name" name="name-input" placeholder="Insert Name of Recipe" onChange={updateForm('name')}></input>
            <p className = 'wrongInput'>{wrongName}</p>
          </div>
          <DropDownTime time = {time} setTime={setTime} setForm={setForm} form={form}/> 
          <p className = 'wrongInput'>{wrongTime}</p>
          <DropDownDifficulty value = {value} setValue={setValue} setForm={setForm} form={form}/>
          <p className = 'wrongInput'>{wrongDifficulty}</p>
          <DropDownCategory category={category} setCategory={setCategory} setForm={setForm} form={form}/>
          <p className = 'wrongInput'>{wrongCategory}</p>
        </div>
      </div>
      <div className='InsertProducts'>
          <textarea id='Products' name='products-input' onChange={updateForm('ingredients')} placeholder={'Insert Ingredients for the Recipe! Example: \n Chicken - 200gr \n Avocado - 500gr \n ...' } ></textarea>
          <p className = 'wrongInput'>{wrongIngredients}</p>
      </div>
      <div className='InsertRecipe'>
          <textarea id='Recipe' name='recipe-input' onChange={updateForm('directions')} placeholder={'Insert Directions! Example: \n Put the Chicken in boiling water for 10 minutes...'}></textarea>
          <p className = 'wrongInput'>{wrongDirections}</p>
      </div>
      <div className='InsertButton'>
      <button className='UploadRecipeButton'  type='submit'onClick={register}>Upload Recipe</button>
      </div>
      </form>
    </div>
  )
}

export default Upload
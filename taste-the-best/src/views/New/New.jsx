import React, { useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom'
import LinksToRecipes from '../../components/LinksToRecipes/LinksToRecipes'
import SearchBar from '../../components/SearchBar/SearchBar'
import Users from '../../components/Users/Users'
import { getAllRecipes } from '../../services/recipes.service'

const New = () => {
  
  const [recipes, setRecipes] = useState([])
  
  useEffect (()=> {
    getAllRecipes()
    .then(setRecipes)
    .catch(console.error)
  }, [])
  recipes.sort(function (a, b) {
    return   b.createdOn - a.createdOn
  })
  //console.log(recipes)
  return (
    <div>
    <Users/>
    <SearchBar/>
    <LinksToRecipes/>
    <div className='Recipes'>
         
         {recipes.length === 0
         ? <p>No recipes to show.</p>
          : recipes.map(recipe =>  {
          return <div key={recipe?.content?.imageURL} className='SingleView'>
            <div className='SingleView'>
            <h3>{recipe?.content?.name}</h3>
            <img src={recipe?.content?.imageURL} alt='recipe' className='SingleImage'></img>
            <NavLink to={`../recipes/all-recipes/${recipe.id}` } className='ViewDetailsLink' >View Details</NavLink>
            </div>
          </div> })
         }
      </div>
 </div>
  )
}

export default New
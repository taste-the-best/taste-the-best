import React from 'react';
import { useContext , useEffect, useState} from 'react';
import { Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import AppContext from '../../providers/AppContext';
import './Profile.css';

const Profile = () => {
    const navigate = useNavigate()
    const {  userData } = useContext(AppContext);
    const [form, setForm] = useState({
      firstName:'',
      lastName:'',
      email:'',
      handle:'',
      profilePicture:''
    })
    const goToEdit=()=>{
      navigate(`/edit-profile`)
    }
    useEffect(()=>{
      setForm({
          email: userData?.email,
          handle: userData?.handle,
          firstName: userData?.firstName,
          lastName: userData?.lastName,
          profilePicture: userData?.profilePicture,
      })
  },[])
  return (
    <div className='DetailsProfileInfo'>
        <div className='LeftProfilePicturePart'>
          <p>Username:<span> {form.handle} </span></p>
          <p>First Name:<span> {form.firstName}</span></p>
          <p>Surname: <span>{form.lastName}</span></p>
          <p>E-mail:<span> {form.email} </span></p>
         
          
          <Button variant='outline-success' className='ProfileEditButton2' onClick={goToEdit}>Edit your profile</Button>{''}
        </div>

        <div className='RightProfilePicturePart'>
   
          <img src={form.profilePicture} alt="profilePicture" className='ProfilePicture'/>
          
        </div>
    </div>
  )
}

export default Profile
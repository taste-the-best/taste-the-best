import React, { useContext, useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import DropDownCategory from '../../components/DropDown/DropDownCategory/DropDownCategory'
import DropDownDifficulty from '../../components/DropDown/DropDownDifficulty/DropDownDifficulty'
import DropDownTime from '../../components/DropDown/DropDownTime/DropDownTime'
import AppContext from '../../providers/AppContext'
import { addRecipe, getRecipeById } from '../../services/recipes.service';
import './Edit.css'


const Edit = () => {
    const [recipe, setRecipe] = useState({})
    const [form, setForm] = useState({
        name: '',
        time: '',
        category: '',
        difficulty: '',
        ingredients:'',
        directions:'',
        imageURL: '',
        likedBy:'',
        dislikedBy:'',
        addedToFavoritesBy:'',
        commentedBy:'',
        commentsIds:'',
      })
    
    
      const navigate = useNavigate()
    const { id } = useParams()
    useEffect(()=>{
        getRecipeById(id)
        .then(setRecipe)
        .catch(console.error)
    },[])
    useEffect(()=>{
        setForm({
            name: recipe?.content?.name,
            time: recipe?.content?.time,
            category: recipe?.content?.category,
            difficulty: recipe?.content?.difficulty,
            ingredients:recipe?.content?.ingredients,
            directions:recipe?.content?.directions,
            imageURL: recipe?.content?.imageURL,
            likedBy:recipe?.content?.likedBy,
            dislikedBy:recipe?.content?.dislikedBy,
            addedToFavoritesBy:recipe?.content?.addedToFavoritesBy,
            commentedBy:recipe?.content?.commentedBy,
            commentsIds:recipe?.content?.commentsIds,
            
        })
    },[recipe?.content?.name, 
      recipe?.content?.time , 
      recipe?.content?.category , 
      recipe?.content?.difficulty, 
      recipe?.content?.ingredients, 
      recipe?.content?.directions , 
      recipe?.content?.imageURL])

    

      const [wrongName, setWrongName] = useState('');
      const [wrongTime, setWrongTime] = useState('');
      const [wrongDifficulty, setWrongDifficulty] = useState('');
      const [wrongCategory, setWrongCategory] = useState('');
      const [wrongIngredients, setWrongIngredients] = useState('');
      const [wrongDirections, setWrongDirections] = useState('');
    
    
      const updateForm = prop => e => {
    
        setForm({
          ...form,
          [prop]: e.target.value,
        });
      };
    
      const register = (e) => {
        e.preventDefault();
    
        if (form.name.length < 5 || form.name.length > 64) {
          setWrongName('* Please add a name between 16 and 64 symbols!')
          return;
        }
        if (form.ingredients.length < 32 || form.ingredients.length > 8192) {
          setWrongIngredients('* Please add ingredients between 32 and 8192 symbols!')
          return;
        }
        if (form.directions.length < 32 || form.directions.length > 8192) {
          setWrongDirections('* Please add directions between 32 and 8192 symbols!')
          return;
        }
        if (form.category === 'Category') {
          setWrongCategory('* Please chose category')
          return;
        }
        if (form.time === 'Select Time') {
          setWrongTime('* Please chose time')
          return;
        }
        if (form.difficulty === 'Difficulty') {
          setWrongDifficulty('* Please chose difficulty')
          return;
        }
        
    
        addRecipe(form, userData.handle, id)
         .then(recipe => {
            setRecipes([recipe, ...recipes])
            
            navigate(`../recipes/all-recipes/${id}`)
           
         })
      };
    
    
      const [time, setTime] = useState('Select Time')
      const [value, setValue] = useState('Difficulty')
      const [category, setCategory] = useState('Category')
      const { userData }= useContext(AppContext)
      const [recipes , setRecipes] = useState([])
      
      

      return (
          <div>
        <div className='Upload' >
            <div className='InsertBar'>
              <div className='NameInput'>
                <input type="text" id="name" name="name-input" value={form.name} placeholder="Insert Name of Recipe" onChange={updateForm('name')}></input>
                <p className = 'wrongInput'>{wrongName}</p>
              </div>
              <DropDownTime time = {form.time} setTime={setTime} setForm={setForm} form={form} /> 
              <p className = 'wrongInput'>{wrongTime}</p>
              <DropDownDifficulty value = {form.difficulty} setValue={setValue} setForm={setForm} form={form}/>
              <p className = 'wrongInput'>{wrongDifficulty}</p>
              <DropDownCategory category={form.category} setCategory={setCategory} setForm={setForm} form={form}/>
              <p className = 'wrongInput'>{wrongCategory}</p>
            </div>
          </div>
          <div className='InsertProducts'>
              <textarea id='Products' name='products-input' onChange={updateForm('ingredients')} value={form.ingredients} placeholder={'Insert Ingredients for the Recipe! Example: \n Chicken - 200gr \n Avocado - 500gr \n ...' } ></textarea>
              <p className = 'wrongInput'>{wrongIngredients}</p>
          </div>
          <div className='InsertRecipe'>
              <textarea id='Recipe' name='recipe-input' onChange={updateForm('directions')} value={form.directions} placeholder={'Insert Directions! Example: \n Put the Chicken in boiling water for 10 minutes...'}></textarea>
              <p className = 'wrongInput'>{wrongDirections}</p>
          </div>
          <div className='InsertButton'>
          <button type='submit' onClick={register} className='EditRecipeButton2'>Edit Recipe</button>
          </div>
        </div>
      )
}

export default Edit
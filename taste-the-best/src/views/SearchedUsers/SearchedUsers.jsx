import React,{useState, useEffect} from 'react'
import { NavLink } from 'react-router-dom';
import { useLocation } from 'react-router-dom';
import SearchedUserBar from '../../components/SearchedUserBar/SearchedUserBar';
import Users from '../../components/Users/Users'
import { getAllUsers } from '../../services/users.service';
import './SearchedUsers.css'

const SearchedUsers = () => {
    const [users, setUsers] = useState([])
    const location = useLocation();
    const test = new URLSearchParams(location.search).get("q")
    
    useEffect(()=>{
            getAllUsers()
            .then((e)=>e.filter(el => {
                if(el === null || el === undefined) {
                  return null
                }else if (el.handle.toLowerCase().includes(test)) {
                  return el
                }else if(el.email.toLowerCase().includes(test)) {
                    return el
                }
              }))
            .then(setUsers)
            .catch(console.error)
    },[test])
  return (
    <div className='SearchedUserContainer'>
    <Users/>
    <SearchedUserBar/>
    {users.map((el,i)=>{
        return <div key={i}>
          <div className="all-users">
            <h2 className='all-users-title'>{el.handle}</h2>
            <img src={el.profilePicture} alt="profile" className='all-users-img'/>
            <NavLink to={`../users/${el.handle}` } className='ViewDetailsToUsersLink' >View Details</NavLink>
          </div>
        </div>
    })}
</div>
  )
}

export default SearchedUsers
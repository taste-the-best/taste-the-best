import React , {useState, useEffect}from 'react'
import { NavLink } from 'react-router-dom';
import { createSearchParams, useNavigate } from 'react-router-dom'
import Users from '../../components/Users/Users'
import { getAllUsers } from '../../services/users.service'
import image from './../../images/search-icon.png'
import './AdminUsers.css'

const AdminUsers = () => {
    const [searchedInput, setSearchedInput] = useState('')
    const navigate = useNavigate();
    const [users, setUsers] = useState([])
    const search = (info) =>{
      
      const result = info.toLowerCase()
      let params = new URLSearchParams(`q=${result}`)
      navigate({pathname:`./search`, search:`${createSearchParams(params)}`})
    }
    
    useEffect(()=>{
            getAllUsers()
            .then(setUsers)
            .catch(console.error)
    },[])
  return (
    <div className='AdminUsersContainer'>
      
        <Users/>
        <div className = "AdminUsersSearch">
            <label> Search: </label>
            <input type="text" id="search" name="search-input" onChange={e=>setSearchedInput(e.target.value)}></input>
            <img src={image} alt="search icon" className="SearchImage" onClick={()=>search(searchedInput)}></img>
        </div>
        {users.map((el,i)=>{
            return <div key={i} className="all-users">
            <div className="all-users">
                <h2 className='all-users-title'>{el.handle}</h2>
                <img src={el.profilePicture} alt="profile" className='all-users-img'/>
                <NavLink to={`../users/${el.handle}` } className='ViewDetailsToUsersLink' >View Details</NavLink>
            </div>
            </div>
        })}
    </div>
  )
}

export default AdminUsers
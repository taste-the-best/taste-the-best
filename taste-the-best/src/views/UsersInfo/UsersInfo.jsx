import React , {useState, useEffect} from 'react'
import { Button } from 'react-bootstrap';
import {  useParams } from 'react-router-dom';
import { getUserByHandle, updateUserRole } from '../../services/users.service';
import './UserInfo.css'

const UsersInfo = () => {
    const [userInfo , setUserInfo] = useState()
    const [userRole, setUserRole] = useState(userInfo?.role)
    const { id } = useParams()
    
    const updateToAdmin = () => {
        updateUserRole(userInfo.handle, 3)
        setUserRole(3)
    }
    const updateToUser = () => {
        updateUserRole(userInfo.handle, 1)
        setUserRole(1)
    }
    const blockPerson = () => {
        updateUserRole(userInfo.handle, 2)
        setUserRole(2)
    }
    useEffect(()=>{
        getUserByHandle(id)
        .then(snapshot => snapshot.val())
        .then(setUserInfo)
        .catch(console.error)
    },[userRole])
    useEffect(()=>{
        getUserByHandle(id)
        .then(snapshot => snapshot.val())
        .then(setUserInfo)
        .catch(console.error)
    },[])
    
  return (
    <div className='DetailsInfoUserContainer'>
        <div className='UserInfoLeftPart'>
            <div>
                <p>Username:<span> {userInfo?.handle} </span></p>
                <p>First Name:<span> {userInfo?.firstName}</span></p>
                <p>Surname: <span>{userInfo?.lastName}</span></p>
                <p>E-mail:<span> {userInfo?.email} </span></p>   
                <p >Liked Recipes: <span>{userInfo?.likedRecipes!== undefined?Object.keys(userInfo?.likedRecipes).length:0}</span></p>
                <p >Disliked Recipes: <span>{userInfo?.dislikedRecipes!== undefined?Object.keys(userInfo?.dislikedRecipes).length:0}</span></p>
                <p >Commented Recipes: <span>{userInfo?.commentedRecipes!== undefined?Object.keys(userInfo?.commentedRecipes).length:0}</span></p>
            </div>
            <div class= 'buttonsForPromo'>
            {userInfo?.role !==3?<Button variant='outline-success' className='EditRoleButton2' onClick={updateToAdmin}>Promote To Admin</Button>:null}
            {userInfo?.role !==1?<Button variant='outline-success' className='EditRoleButton2' onClick={updateToUser}>Demote To User</Button>:null}
            {userInfo?.role !==2?<Button variant='outline-success' className='EditRoleButton2' onClick={blockPerson}>Block Person</Button>:null}
            </div>
        </div>
        <div className='PictureOfUser'>
            <img src={userInfo?.profilePicture} alt="profilePicture" className='ProfilePicture'/>
        </div>
       
    </div>
  )
}

export default UsersInfo
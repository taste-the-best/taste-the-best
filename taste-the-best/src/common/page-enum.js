export const pages = {
    Home: 'home',
    Recipes: 'recipes',
    Upload: 'upload',
    About: 'about',
    Profile: 'profile',
    LogIn: 'login',
    LogOut: 'logout',
    Register: 'register',
    AllRecipes: 'all-recipes'
  };
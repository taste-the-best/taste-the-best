import { useContext } from 'react';
import AppContext from '../providers/AppContext';
import { Navigate, useLocation } from 'react-router-dom';

export default function Authenticated ({ children, loading }) { //  /tweets
  const { user } = useContext(AppContext);
  const location = useLocation();

  if (loading) {
    return <h1>Loading...</h1>
  }

  if (!user) {
    return <Navigate to="/home" state={{ from: location }} />
  }

  return children;
}


import './App.css';
import Header from './components/Header/Header';
import Home from './views/Home/Home';
import About from './views/About/About'
import MyRecipes from './views/MyRecipes/MyRecipes'
import Upload from './views/Upload/Upload'
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import Footer from './components/Footer/Footer';
import AllRecipes from './views/AllRecipes/AllRecipes';
import Favorites from './views/Favorites/Favorites';
import MostPopular from './views/MostPopular/MostPopular';
import New from './views/New/New';
import 'bootstrap/dist/css/bootstrap.min.css';
import ViewDetails from './views/ViewDetails/ViewDetails';
import AppContext from './providers/AppContext';
import { useEffect, useState } from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from './config/firebase-config';
import { getUserData } from './services/users.service';
import Register from './views/Register/Register';
import Profile from './views/Profile/Profile';
import Login from './views/Login/Login'
import Searched from './views/Searched/Searched';
import Category from './views/Category/Category';
import Edit from './views/Edit/Edit';
import Authenticated from './hoc/Authenticated';
import EditProfile from './views/EditProfile/EditProfile';
import AdminUsers from './views/AdminUsers/AdminUsers';
import SearchedUsers from './views/SearchedUsers/SearchedUsers';
import UsersInfo from './views/UsersInfo/UsersInfo'



function App() {

  const [appState, setAppState] = useState({
    user: null,
    userData: null,
  });

  let [user] = useAuthState(auth);

  useEffect(() => {
    if (user === null) return;

    getUserData(user.uid)
      .then(snapshot => {
        if (!snapshot.exists()) {
          throw new Error('Something went wrong!');
        }

        setAppState({
          user,
          userData: snapshot.val()[Object.keys(snapshot.val())[0]],
        });


      })
      .catch(e => console.log(e.message));
  }, [user]);


  return (
    <BrowserRouter>
      <AppContext.Provider value={{...appState, setContext: setAppState}}>
        <div className="App">
          <Header/>
          <div className='Outlet'>
            <Routes>
            <Route index element={<Navigate replace to="home" />} />
              <Route path="home" element ={<Home/>}></Route>
              <Route path="recipes" index element ={<Navigate replace to="all-recipes"/>}></Route>
              <Route path="recipes/all-recipes" element={<AllRecipes/>}></Route>
              <Route path="recipes/favorites" element={<Favorites/>}></Route>
              <Route path="recipes/most-popular" element={<MostPopular/>}></Route>
              <Route path="recipes/new-recipes" element={<New/>}></Route>
              <Route path="my-recipes" element={<MyRecipes/>}></Route>
              <Route path="upload" element={<Upload/>}></Route>
              <Route path="edit/:id" element={<Edit/>}></Route>
              <Route path="edit-profile" element={<EditProfile/>}></Route>
              <Route path="recipes/search" element={<Searched />}/>
              <Route path="recipes/category" element={<Category />}/>
              <Route path="users" element={<AdminUsers />}/>
              <Route path="users/search" element={<SearchedUsers/>}/>
              <Route path="/users/:id" element={<UsersInfo/>}></Route>
              <Route path="about" element={<About />} />
              <Route path="register" element={<Register />} />
              <Route path="login" element={<Login />} />
              <Route path="profile" element={<Profile />} />
              <Route path="recipes/all-recipes/:id" element={<Authenticated> <ViewDetails /> </Authenticated>} ></Route>
              
              
              
            </Routes>
          </div>
          <footer>
            <Footer/>
          </footer>
        </div>
      </AppContext.Provider>
    </BrowserRouter>
  );
}

export default App;

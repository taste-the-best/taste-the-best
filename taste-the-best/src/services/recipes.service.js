import { ref, get, query, equalTo, orderByChild, update, onValue , set} from 'firebase/database';
import { db } from '../config/firebase-config';


export const fromRecipesDocument = snapshot => {
  const recipesDocument = snapshot.val();

  return Object.keys(recipesDocument).map(key => {
    const recipe = recipesDocument[key];

    return {
      ...recipe,
      id: key,
      createdOn: new Date(recipe.createdOn),
      likedBy: recipe.likedBy ? Object.keys(recipe.likedBy) : [],

    };
  });
};

export const addRecipe = async (content, handle , id) => {

  return await set(
    ref(db, `recipes/${id}`),
    {
      content,
      author: handle,
      createdOn: Date.now(),
    },
  )
    .then(result => {

      return getRecipeById(id);
    });
};

export const getRecipeById = async (id) => {

  return await get(ref(db, `recipes/${id}`))
    .then(result => {
      if (!result.exists()) {
        throw new Error(`Recipe with id ${id} does not exist!`);
      }

      const recipe = result.val();
      recipe.id = id;
      recipe.createdOn = new Date(recipe.createdOn);
      if (!recipe.likedBy) {
        recipe.likedBy = [];
      } else {
        recipe.likedBy = Object.keys(recipe.likedBy);
      }

      return recipe;
    });
};

export const getLikedRecipe = async (handle) => {

  return await get(ref(db, `users/${handle}`))
    .then(snapshot => {
      if (!snapshot.val()) {
        throw new Error(`User with handle @${handle} does not exist!`);
      }

      const user = snapshot.val();
      if (!user.likedRecipes) return [];

      return Promise.all(Object.keys(user.likedRecipes).map(key => {

        return get(ref(db, `recipes/${key}`))
          .then(snapshot => {
            const recipe = snapshot.val();

            return {
              ...recipe,
              createdOn: new Date(recipe.createdOn),
              id: key,
              likedBy: recipe.likedBy ? Object.keys(recipe.likedBy) : [],
            };
          });
      }));
    });
};

export const getRecipesByAuthor = async (handle) => {

  return await get(query(ref(db, 'recipes'), orderByChild('author'), equalTo(handle)))
    .then(snapshot => {
      if (!snapshot.exists()) return [];

      return fromRecipesDocument(snapshot);
    });
};

export const getLiveRecipes = (listen) => {
  return onValue(ref(db, 'recipes'), listen);
}

export const getAllRecipes = async () => {

  return await get(ref(db, 'recipes'))
    .then(snapshot => {
      if (!snapshot.exists()) {
        return [];
      }

      return fromRecipesDocument(snapshot);
    });
};

export const likeRecipes = (handle, recipeId) => {
  const updateLikes = {};
  updateLikes[`/recipes/${recipeId}/content/likedBy/${handle}`] = true;
  updateLikes[`/recipes/${recipeId}/content/dislikedBy/${handle}`] = null;
  updateLikes[`/users/${handle}/likedRecipes/${recipeId}`] = true;
  updateLikes[`/users/${handle}/dislikedRecipes/${recipeId}`] = null;

  return update(ref(db), updateLikes);
};

export const removeLikeRecipe = (handle, recipeId) => {
  const updateLikes = {};
  updateLikes[`/recipes/${recipeId}/content/likedBy/${handle}`] = null;
  updateLikes[`/users/${handle}/likedRecipes/${recipeId}`] = null;

  return update(ref(db), updateLikes);
};

export const dislikeRecipes = (handle, recipeId) => {
  const updateLikes = {};
  updateLikes[`/recipes/${recipeId}/content/dislikedBy/${handle}`] = true;
  updateLikes[`/recipes/${recipeId}/content/likedBy/${handle}`] = null;
  updateLikes[`/users/${handle}/dislikedRecipes/${recipeId}`] = true;
  updateLikes[`/users/${handle}/likedRecipes/${recipeId}`] = null;

  return update(ref(db), updateLikes);
};

export const removeDislikeRecipe = (handle, recipeId) => {
  const updateLikes = {};
  updateLikes[`/recipes/${recipeId}/content/dislikedBy/${handle}`] = null;
  updateLikes[`/users/${handle}/dislikedRecipes/${recipeId}`] = null;

  return update(ref(db), updateLikes);
};

export const deleteRecipe = async (id) => {
  const recipe = await getRecipeById(id);
  const updateLikes = {};

  recipe.likedBy.forEach(handle => {
    updateLikes[`/users/${handle}/likedRecipes/${id}`] = null;
  });

  await update(ref(db), updateLikes);

  return update(ref(db), {
    [`/recipes/${id}`]: null,
  });
};


export const fromLikedDocument = (snapshot) => {
  const usersDocument = snapshot.val()
  return Object.keys(usersDocument).map(key => {
    const user1 = usersDocument[key];
    return {
      ...user1
    }
  })
}
export const getAllLiked = async (id) => {
  return await get(ref(db, `/recipes/${id}/content/likedBy`))
  .then(snapshot => {    
    if (!snapshot.exists()) {
      return [];
    }
    
    return fromLikedDocument(snapshot);
  })
};

export const fromDislikedDocument = (snapshot) => {
  const usersDocument = snapshot.val()
  return Object.keys(usersDocument).map(key => {
    const user1 = usersDocument[key];
    return {
      ...user1
    }
  })
}
export const getAllDisliked = async (id) => {
  return await get(ref(db, `/recipes/${id}/content/dislikedBy`))
  .then(snapshot => {    
    if (!snapshot.exists()) {
      return [];
    }

    return fromLikedDocument(snapshot);
  })
};


export const getLiked = async (id, handle) => {
  return await get(ref(db, `/recipes/${id}/content/likedBy/${handle}`))
  .then(snapshot => {    
    if (!snapshot.exists()) {
      return [];
    } 
    return fromLikedDocument(snapshot);
  })
};


export const getDisliked = async (id, handle) => {
  return await get(ref(db, `/recipes/${id}/content/dislikedBy/${handle}`))
  .then(snapshot => {    
    if (!snapshot.exists()) {
      return [];
    }
    
    return fromDislikedDocument(snapshot);
  })
};

export const getFavoriteStatus = async (id, handle) => {
  return await get(ref(db, `/recipes/${id}/content/addedToFavoritesBy/${handle}`))
  .then(snapshot => {
    if (snapshot.exists()) {
      return true;
    } else {
      return false;
    }

  })
};

export const checkIsLikedByHandle = async (recipeId, handle) => {
  return await get(ref(db, `/recipes/${recipeId}/content/likedBy/${handle}`))
  .then(snapshot => {
    if (snapshot.exists()) {
      return true;
    } else {
      return false;
    }
  })
};

export const checkIsDislikedByHandle = async (recipeId, handle) => {
  return await get(ref(db, `/recipes/${recipeId}/content/dislikedBy/${handle}`))
  .then(snapshot => {
    if (snapshot.exists()) {
      return true;
    } else {
      return false;
    }
  })
};
export const fromSortedDocument = (snapshot) => {
  const usersDocument = snapshot.val()
  return Object.keys(usersDocument).map(key => {
    const user1 = usersDocument[key];
    return {
      ...user1
    }
  })
}

export const setRatingDataFunction = (recipeId, rating) => {
   const updateRating = {};
   updateRating[`/recipes/${recipeId}/content/rating`] = rating;

   return update(ref(db), updateRating);
};


export const getRating = async (recipeId) => {
  return await get(ref(db, `/recipes/${recipeId}/content/rating`))
  .then(snapshot => {
    if (snapshot.exists()) {
      return snapshot.val();
    } else {
      return 0;
    }

  })
};


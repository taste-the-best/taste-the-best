import { ref, push, get, query, equalTo, orderByChild, update, onValue , set, orderByValue} from 'firebase/database';
import { db } from '../config/firebase-config';
export const fromCommentsDocument = async snapshot => {
    const commentIds = Object.keys(snapshot.val());
  
    return await Promise.all(commentIds.map(id => {
      return getCommentById(id);
    }));
  };
  
  export const addComment = async (comment, handle, idOfComment, idOfRecipe, profilePictureURL) => {

    return await set(
      ref(db, `comments/${idOfComment}`),
      {
        content:{
          textComment: comment,
          recipeId: idOfRecipe,
          profilePictureURL: profilePictureURL,
        }, 
        author: handle,
        createdOn: Date.now(),
      },
    )
      .then(result => {
        return getCommentById(idOfComment);
      });
  };
  
  export const getCommentById = async (id) => {

    return await get(ref(db, `comments/${id}`))
      .then(result => {
        if (!result.exists()) {
          throw new Error(`Comment with id ${id} does not exist!`);
        }
  
        const comment = result.val();
        comment.id = id;
        comment.createdOn = new Date(comment.createdOn);
        if (!comment.likedBy) {
          comment.likedBy = [];
        } else {
          comment.likedBy = Object.keys(comment.likedBy);
        }
  
        return comment;
      });
  };

  export const getCommentsByRecipe = async (id) => {
    return await get(ref(db, `recipes/${id}`))
    .then (result => {
        if(!result.exists()) {
            throw new Error(`Recipe with id ${id} does not exist!`);
        }

        const comment = result.val();
        comment.id = id;
        comment.createdOn = new Date(comment.createdOn);
        if (!comment.likedBy) {
          comment.likedBy = [];
        } else {
          comment.likedBy = Object.keys(comment.likedBy);
        }
  
        return comment;
    })
  }
  export const getCommentsByAuthor = async (handle) => {

    return await get(query(ref(db, 'comments'), orderByChild('author'), equalTo(handle)))
      .then(snapshot => {
        if (!snapshot.exists()) return [];
  
        return fromCommentsDocument(snapshot);
      });
  };

  export const commentRecipes = (handle, commentId, recipeId) => {
    const updateComments = {};
    updateComments[`/recipes/${recipeId}/content/commentedBy/${handle}`] = true;
    updateComments[`/recipes/${recipeId}/content/commentsIds/${commentId}`] = true;
    updateComments[`/users/${handle}/commentedRecipes/${recipeId}`] = true;

    return update(ref(db), updateComments);
  };


  export const getAllComments = async (recipeId) => {
    return await get(ref(db, `recipes/${recipeId}/content/commentsIds`))
      .then(snapshot => {
        if (!snapshot.exists()) {
          return [];
        }

        return fromCommentsDocument(snapshot);
      });
  };


  export const deleteComment = async (commentId, recipeId, handle) => {
  
    return update(ref(db), {
      [`/comments/${commentId}`]: null,
      [`/recipes/${recipeId}/content/commentsIds/${commentId}`]: null,
      
    });
  };


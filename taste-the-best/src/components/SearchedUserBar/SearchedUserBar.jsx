import React,{useState, useEffect} from 'react'
import { createSearchParams, useNavigate } from 'react-router-dom';
import image from './../../images/search-icon.png'

const SearchedUserBar = () => {
    const [searchedInput, setSearchedInput] = useState('')
    const navigate = useNavigate();
    const search = (info) =>{
      
        const result = info.toLowerCase()
        let params = new URLSearchParams(`q=${result}`)
        navigate({pathname:`./../search`, search:`${createSearchParams(params)}`})
      }
  return (
    <div className = "Search">
    <label> Search: </label>
    <input type="text" id="search" name="search-input" onChange={e=>setSearchedInput(e.target.value)}></input>
    <img src={image} alt="search icon" className="SearchImage" onClick={()=>search(searchedInput)}></img>
</div>
  )
}

export default SearchedUserBar
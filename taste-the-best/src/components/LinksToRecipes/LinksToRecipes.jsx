import './LinksToRecipes.css'
import React, { useEffect, useState } from 'react';
import { createSearchParams, NavLink, useNavigate } from 'react-router-dom';
import { Dropdown } from 'react-bootstrap';
import DropDownCategory from '../DropDown/DropDownCategory/DropDownCategory';

const LinksToRecipes = () => {
  const [category, setCategory] = useState('Category')
  const [form, setForm] = useState('')
  const navigate = useNavigate();
  useEffect((e)=>{
   
    if(category !== 'Category') {
      let params = new URLSearchParams(`q=${category}`)
    navigate({pathname:`./../category`, search:`${createSearchParams(params)}`})
    // navigate(`/category`, {state:{category: category}})
    }
  },[category])
 
  return (
    <div>

      <DropDownCategory category={category} setCategory={setCategory} setForm={setForm} form={form}/>
       
       <div className='NavLinksContainer'>

        <NavLink to={'../recipes/all-recipes'} className="navLink">All recipes</NavLink>
        <NavLink to={'../recipes/favorites'} className="navLink" >Favorites</NavLink>
        <NavLink to={'../recipes/most-popular'} className="navLink">Most Popular</NavLink>
        <NavLink to={'../recipes/new-recipes'} className="navLink">New Recipes</NavLink>

       </div>
    </div>
  )
}

export default LinksToRecipes

import './AllComments.css';
import Comment from '../Comment/Comment';

const AllComments = ({comments, setComments}) => {

  comments.sort(function (a, b) {
    return   b.createdOn - a.createdOn
  })


  return (
    <div className='CommentsContainer'>   
        {comments.length === 0
        ? <h2>No comments to show.</h2>
        
        : comments.map((comment, key) => 
        <div key={key} className='SingleComment'>
            <div className='SingleComment2'>
            <Comment key={key} comment={comment} comments = {comments} setComments={setComments} />
            </div>
        </div>)
        }
    </div>
  );
};

export default AllComments;

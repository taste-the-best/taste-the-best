import './Recipe.css';
import { useContext } from 'react';
import AppContext from '../../providers/AppContext';

/**
 * 
 * @param {{ recipe: { id: string, content: string, author: string, date: Date, likedBy: string[] } } } recipe 
 */
const Recipe = ({ recipe }) => {
 /// const { userData: { handle } } = useContext(AppContext);

  //const isRecipeLiked = () => recipe.likedBy.includes(handle);

  return (
    <div className='Recipe'>
      <div className='Recipe-Header'>{recipe.author}</div>
      <div className='Recipe-Content'>{recipe.content}</div>
      <div className='Recipe-Meta'>
        
        
        
        </div>
    </div>
  )
};

export default Recipe;

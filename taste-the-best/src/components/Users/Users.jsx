import React, { useEffect, useState } from 'react'
import { getAllRecipes } from '../../services/recipes.service'
import { getAllUsers } from '../../services/users.service'
import './Users.css'

const Users = () => {
  const [users, setUsers] = useState([])
  const [recipes, setRecipes] = useState([])

  useEffect (() => {
    getAllUsers()
    .then(setUsers)
    .catch(console.error)

    getAllRecipes()
    .then(setRecipes)
    .catch(console.error)
  }, [])

 


  
  return (
    <div className="Users">
        <div className="UsersCounts">Number of Users: {users.length}</div>   
        <div className="PostsCounts">Number of Recipes: {recipes.length}</div>
    </div>
  )
}

export default Users
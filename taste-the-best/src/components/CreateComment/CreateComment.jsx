import { useState } from 'react';
import './CreateComment.css';
import Button from 'react-bootstrap/Button';

const CreateComment = ({ onSubmit }) => {
    const [content, setContent] = useState('');
  
    const createComment = () => {
      onSubmit(content)
        .then(() => {
          setContent('');
          
        })
        .catch(e => alert(e.message));
    };

    return (
      <div className='CreateComment'>
        <textarea placeholder='Write your comment here...' value={content} onChange={e => setContent(e.target.value)}>
  
        </textarea><br />
        <Button variant="success" className='CreateCommentButton' onClick={createComment}>Comment</Button>
      </div>
    );
  };
  
  export default CreateComment;
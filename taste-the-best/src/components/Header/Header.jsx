import React from 'react';
import { NavLink } from 'react-router-dom';
import image from './../../../src/images/header-image-pizza-7.png';
import './Header.css';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from '../../config/firebase-config';
import { logoutUser } from '../../services/auth.service';
import { useContext } from 'react';
import AppContext from '../../providers/AppContext';
import Button from 'react-bootstrap/Button';
import { useNavigate } from 'react-router-dom';




const Header = () => {

  const { setContext, ...appState } = useContext(AppContext);
  let [user, loading, error] = useAuthState(auth);
  const navigate = useNavigate();
  const { userData }= useContext(AppContext)

  const logout = () => {
    logoutUser()
      .then(() => {
        navigate('/home');
        setContext({ user: null, userData: null });
      });
  };

 
  return (
    <header className = "Header">
      <div className = "HeaderImage">
        <img src={image} alt="pizza" />
      </div>
      <nav>
        <NavLink to="/home">Home</NavLink>  
        

        { loading
            ? <p>Loading user data</p>
            : <>
       
              {appState.user === null
              ? <> 
                  <NavLink to="/about">About</NavLink>
                  <NavLink to="/register">Register</NavLink>
                  <NavLink to="/login">LogIn</NavLink>
      
              </> :   userData.role ===1 || userData.role ===2         
                ?<> 
                <NavLink to="/recipes">Recipes</NavLink> 
                {userData.role !==2?<NavLink to="/my-recipes">My Recipes</NavLink>:null}
                {userData.role !==2?<NavLink to="/upload">Upload</NavLink>:null}
                <NavLink to="/profile">Profile</NavLink>
                <NavLink to="/about">About</NavLink>
                <Button variant="outline-success" id="UploadBtn" onClick={logout}>Logout</Button>
                </>
                :<>
                 <NavLink to="/recipes">Recipes</NavLink> 
                <NavLink to="/my-recipes">My Recipes</NavLink>
                <NavLink to="/upload">Upload</NavLink>
                <NavLink to="/profile">Profile</NavLink>
                <NavLink to="/users">Users</NavLink>
                <NavLink to="/about">About</NavLink>
                <Button variant="outline-success" id="UploadBtn" onClick={logout}>Logout</Button>
                </>
            
}
            </>}
        
        
      </nav>
    </header>
   
  )
}

export default Header
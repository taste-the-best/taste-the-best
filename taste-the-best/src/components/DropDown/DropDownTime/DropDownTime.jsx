import './DropDownTime.css'
import React, { useState } from 'react'
import { Dropdown } from 'react-bootstrap';

const DropDownTime = (props) => {
  const {time , setTime, setForm , form} = props

    const handleSelectTime = (e) => {
        setTime(e)
        setForm({
          ...form,
          time: e
        })   
    }
  return (
    <div className= "DropDownTime">
          <Dropdown onSelect={handleSelectTime}>
            <Dropdown.Toggle variant="success" id="dropdown-basic" >
              {time}
            </Dropdown.Toggle>

            <Dropdown.Menu id="dropdown-items">
            <Dropdown.Item  eventKey={'Less than 10 minutes'}>Less than 10 minutes</Dropdown.Item>
              <Dropdown.Item  eventKey={'10 to 20 minutes'}>10 to 20 minutes</Dropdown.Item>
              <Dropdown.Item  eventKey={'20 to 30 minutes'}>20 to 30 minutes</Dropdown.Item>
              <Dropdown.Item  eventKey={'30 to 60 minutes'}>30 to 60 minutes</Dropdown.Item>
              <Dropdown.Item  eventKey={'60 to 90 minutes'}>60 to 90 minutes</Dropdown.Item>
              <Dropdown.Item  eventKey={'90 to 120 minutes'}>90 to 120 minutes</Dropdown.Item>
              <Dropdown.Item  eventKey={'More than 120 minutes'}>More than 120 minutes</Dropdown.Item>
            </Dropdown.Menu>
            
          </Dropdown>
        </div>
  )
}

export default DropDownTime
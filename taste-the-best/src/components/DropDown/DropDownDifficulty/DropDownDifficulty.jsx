import './DropDownDifficulty.css'
import React, { useState } from 'react'
import { Dropdown } from 'react-bootstrap';

const DropDownDifficulty = (props) => {
    const {value , setValue , setForm , form} = props
    
    const handleSelect = (e)=>{
        setValue(e)
        setForm({
          ...form,
          difficulty: e
        })   
      }
  return (
    <div className= "DropDownDifficulty">
        <Dropdown onSelect={handleSelect}>
            <Dropdown.Toggle variant="success" id="dropdown-basic" >
               {value}
            </Dropdown.Toggle>

            <Dropdown.Menu id="dropdown-items">
              <Dropdown.Item  eventKey={'Easy'}>Easy</Dropdown.Item>
              <Dropdown.Item  eventKey={'Medium'}>Medium</Dropdown.Item>
              <Dropdown.Item  eventKey={'Hard'}>Hard</Dropdown.Item>
            </Dropdown.Menu>
            
          </Dropdown>
        </div>
  )
}

export default DropDownDifficulty
import './DropDownCategory.css'
import React, { useState } from 'react'
import { Dropdown } from 'react-bootstrap';

const DropDownCategory = (props) => {
    const {category, setCategory , setForm , form} = props
    
    const handleSelectCategory = (e) => {
      setCategory(e)
      setForm({
        ...form,
        category: e
      })    
    }
  return (
    <div className= "DropDownCategory">
          <Dropdown onSelect={handleSelectCategory}>
            <Dropdown.Toggle variant="success" id="dropdown-basic" >
              {category}
            </Dropdown.Toggle>

            <Dropdown.Menu id="dropdown-items">
              <Dropdown.Item  eventKey={'Salad'}>Salad</Dropdown.Item>
              <Dropdown.Item  eventKey={'Soups'}>Soups</Dropdown.Item>
              <Dropdown.Item  eventKey={'Fish'}>Fish</Dropdown.Item>
              <Dropdown.Item  eventKey={'Chicken'}>Chicken</Dropdown.Item>
              <Dropdown.Item  eventKey={'Pork'}>Pork</Dropdown.Item>
              <Dropdown.Item  eventKey={'Beef'}>Beef</Dropdown.Item>
              <Dropdown.Item  eventKey={'Lamb'}>Lamb</Dropdown.Item>
              <Dropdown.Item  eventKey={'Bakery'}>Bakery</Dropdown.Item>
              <Dropdown.Item  eventKey={'Desert'}>Desert</Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item  eventKey={'Vegan'}>Vegan</Dropdown.Item>
              <Dropdown.Item  eventKey={'Vegetarian'}>Vegetarian</Dropdown.Item>

            </Dropdown.Menu>
            
          </Dropdown>
        </div>
  )
}

export default DropDownCategory
import { useState } from 'react';
import './CreateRecipe.css';

/**
 * 
 * @param {{ onSubmit: (content: string) => Promise<any>}} param0 
 * @returns 
 */
const CreateRecipe = ({ onSubmit }) => {
  const [content, setContent] = useState('');

  const createRecipe = () => {
    onSubmit(content)
      .then(() => {
        setContent('');
      
      })
      .catch(e => alert(e.message));
  };

  return (
    <div className='CreateRecipe'>
      <textarea value={content} onChange={e => setContent(e.target.value)}>

      </textarea><br />
      <button onClick={createRecipe}>Recipe</button>
    </div>
  );
};

export default CreateRecipe;

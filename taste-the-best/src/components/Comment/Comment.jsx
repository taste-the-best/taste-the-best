import './Comment.css';
import { deleteComment} from '../../services/comments.service';
import { useContext , useEffect, useState} from 'react';
import AppContext from '../../providers/AppContext';
import { getUserByHandle } from '../../services/users.service';


const Comment = ({ comment, comments, setComments }) => {
  const { userData: { handle, profilePicture , role} } = useContext(AppContext);
  const [userInfo, setUserInfo] = useState()

    const date = comment?.createdOn;
    let commentDate = '';
    if (date !== undefined) {
       commentDate = date.toString().split(' ').splice(0, 5).join(' ');
    }
    useEffect(()=>{
      getUserByHandle(comment.author)
      .then(snapshot => snapshot.val())
      .then(setUserInfo)
      .catch(console.error)
  },[])

  return (
    <div className='Comment'>
      <div className='CommentLeft'>
        <img src={userInfo?.profilePicture} alt='profile' className='CommentProfilePicture'></img>
      </div>
      <div className='CommentRight'>
        <div className='CommentAuthor'>Author: <span>{comment.author}</span></div>
        <div className='CommentText'>
          <p> {comment.content.textComment}</p>
          
        </div>
        <p className='CreatedOn'>Created on: {commentDate}</p>
        <div className='CommentLikes'>

        </div>
        <div className ='DeleteComment'>
        {
        comment.author === handle || role === 3
          ? <button  id='DeleteCommentButton' onClick={
              () => {
                deleteComment(comment.id, comment.content.recipeId, handle);
                setComments(comments.filter(c => c.id !== comment.id));
              }
            }>Delete</button> 
          : null 
        }
          
        </div>
      </div>
    </div>
  )
};

export default Comment;

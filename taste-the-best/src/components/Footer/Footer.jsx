import React from 'react'
import './Footer.css'

const Footer = () => {
  return (
    <div className='footer'>Reproduction in whole or in part without written permission is strictly prohibited.<br></br>
    © Copyright 2022 Taste the Best! All Rights Reserved!</div>
  )
}

export default Footer
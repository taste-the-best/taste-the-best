import React from 'react'
import SingleTopContainer from '../SingleTopContainer/SingleTopContainer'

const Tops = (props) => {
  const result = props.arr.map((el)=>SingleTopContainer(el))

  return (
    <div className='TopContainer'>
      {result}
    </div>
  )
}
Tops.defaultProps= {
  arr: [1,2,3,4]
}

export default Tops
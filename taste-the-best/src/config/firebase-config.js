import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getFirestore } from '@firebase/firestore';
import { getDatabase } from '@firebase/database';
import { getStorage } from 'firebase/storage';






const firebaseConfig = {
  apiKey: "AIzaSyAseOOU616HGHI_dBSh2DONNhPxGyDIwxQ",
  authDomain: "taste-the-best-app.firebaseapp.com",
  databaseURL: "https://taste-the-best-app-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "taste-the-best-app",
  storageBucket: "taste-the-best-app.appspot.com",
  messagingSenderId: "176848421742",
  appId: "1:176848421742:web:7ce49ebda48cbf8cd0b77f",
  measurementId: "G-9GGGS9CN04"
};

  export const app = initializeApp(firebaseConfig);
  export const auth = getAuth(app);
  export const firestore = getFirestore(app);
  export const db = getDatabase(app);

  export const storage = getStorage(app);

